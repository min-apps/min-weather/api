const AWS = require("aws-sdk");
const fs = require("fs");

AWS.config.update({ region: "us-east-1" });
const ddb = new AWS.DynamoDB({ apiVersion: "2012-10-08" });

const cities = JSON.parse(fs.readFileSync("city-list.json", "utf8"));

const cityPutRequests = cities
  .map((city) => {
    return {
      name: {
        S: city.name,
      },
      id: {
        N: city.id,
      },
      country: {
        S: city.country,
      },
      longitude: {
        N: city.coord.lon,
      },
      latitude: {
        N: city.coord.lat,
      },
    };
  })
  .map((row) => {
    return {
      PutRequest: {
        Item: row,
      },
    };
  })
  .reduce((chunkedPutRequests, putRequest, currentIndex, source) => {
    if (currentIndex % 25 !== 0) {
      return chunkedPutRequests;
    }
    chunkedPutRequests.push(source.slice(currentIndex, currentIndex + 25));
    return chunkedPutRequests;
  }, []);

console.log(JSON.stringify(cityPutRequests.slice(-1)[0]));
console.log(cityPutRequests.slice(-1)[0].length);
console.log(cityPutRequests.length);
