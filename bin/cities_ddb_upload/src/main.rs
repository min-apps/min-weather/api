#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate log;

extern crate env_logger;
extern crate rusoto_core;
extern crate rusoto_dynamodb;
extern crate serde;
extern crate serde_json;

use rusoto_core::Region;
use rusoto_dynamodb::{
    AttributeValue, BatchWriteItemError::*, BatchWriteItemInput, DynamoDb, DynamoDbClient,
    PutRequest, WriteRequest,
};
use std::collections::HashMap;
use std::fs;

#[derive(Serialize, Deserialize)]
struct City {
    id: i64,
    name: String,
    country: String,
    coord: Coordinates,
}

#[derive(Serialize, Deserialize)]
struct Coordinates {
    lon: f64,
    lat: f64,
}

fn main() {
    const MAX_DDB_WRITE_REQUESTS: usize = 25;
    let _ = env_logger::try_init();
    let cities = parse(&read_file("city-list.json"));

    let city_write_requests = cities
        .iter()
        .map(|city| {
            let name = AttributeValue {
                s: Some(city.name.clone()),
                ..Default::default()
            };
            let id = AttributeValue {
                n: Some(format!("{}", city.id)),
                ..Default::default()
            };
            let country = AttributeValue {
                s: Some(city.country.clone()),
                ..Default::default()
            };
            let longitude = AttributeValue {
                n: Some(format!("{}", city.coord.lon)),
                ..Default::default()
            };
            let latitude = AttributeValue {
                n: Some(format!("{}", city.coord.lat)),
                ..Default::default()
            };

            let mut attributes = HashMap::new();
            attributes.insert("name".to_string(), name);
            attributes.insert("id".to_string(), id);
            attributes.insert("country".to_string(), country);
            attributes.insert("longitutude".to_string(), longitude);
            attributes.insert("latitude".to_string(), latitude);

            return attributes;
        }).map(|row| {
            let write_request = WriteRequest {
                put_request: Some(PutRequest { item: row }),
                ..Default::default()
            };

            return write_request;
        }).collect::<Vec<WriteRequest>>();

    println!(
        "Attempting to upload {} cities to DynamoDB.",
        city_write_requests.len()
    );

    let dynamodb_client = DynamoDbClient::new(Region::UsEast1);
    city_write_requests
        .chunks(MAX_DDB_WRITE_REQUESTS)
        .map(|chunk| {
            let mut request_items = HashMap::new();
            request_items.insert(
                "min-weather-api-CitiesTable-dev-1".to_string(),
                chunk.to_vec(),
            );
            return request_items;
        }).map(|request| {
            return BatchWriteItemInput {
                request_items: request,
                ..Default::default()
            };
        }).for_each(|request| {
            let result = dynamodb_client.batch_write_item(request).sync();
            match result {
                Ok(message) => println!("OK: {:?}", message),
                Err(error) => match error {
                    Unknown(e) => println!("Unknown error: {:?}", e),
                    InternalServerError(e) => println!("InternalServerError error: {:?}", e),
                    ItemCollectionSizeLimitExceeded(e) => {
                        println!("ItemCollectionSizeLimitExceeded error: {:?}", e)
                    }
                    ProvisionedThroughputExceeded(e) => {
                        println!("ProvisionedThroughputExceeded error: {:?}", e)
                    }
                    ResourceNotFound(e) => println!("ResourceNotFound error: {:?}", e),
                    HttpDispatch(e) => println!("HttpDispatch error: {:?}", e),
                    Credentials(e) => println!("Credentials error: {:?}", e),
                    Validation(e) => println!("Validation error: {:?}", e),
                },
            }
        });
}

fn batch_write(request: BatchWriteItemInput) {
    let result = dynamodb_client.batch_write_item(request).sync();
    match result {
        Ok(message) => println!("OK: {:?}", message),
        Err(error) => match error {
            Unknown(e) => println!("Unknown error: {:?}", e),
            InternalServerError(e) => println!("InternalServerError error: {:?}", e),
            ItemCollectionSizeLimitExceeded(e) => {
                println!("ItemCollectionSizeLimitExceeded error: {:?}", e)
            }
            ProvisionedThroughputExceeded(e) => {
                println!("ProvisionedThroughputExceeded error: {:?}", e)
                batch_write(request);
            }
            ResourceNotFound(e) => println!("ResourceNotFound error: {:?}", e),
            HttpDispatch(e) => println!("HttpDispatch error: {:?}", e),
            Credentials(e) => println!("Credentials error: {:?}", e),
            Validation(e) => println!("Validation error: {:?}", e),
        },
    }
}

fn read_file(filename: &str) -> String {
    return match fs::read_to_string(filename) {
        Ok(contents) => contents,
        Err(error) => panic!("Failed to read file {}: {:?}", filename, error),
    };
}

fn parse(contents: &String) -> Vec<City> {
    return match serde_json::from_str(&contents) {
        Ok(parsed_cities) => parsed_cities,
        Err(error) => panic!("There was an error parsing contents: {:?}", error),
    };
}
