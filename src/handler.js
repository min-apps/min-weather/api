import axios from "axios";

export const graphqlHandler = async (event, context, callback) => {
  console.log(`Received request ${JSON.stringify(event, 3)}`);
  if (event.field === "getCurrentWeather") {
    await axios
      .get("https://api.openweathermap.org/data/2.5/weather", {
        params: {
          id: "4280539",
          APPID: "1b625d5c0e13b69409566ac948002136",
          units: "metric",
        },
      })
      .then((response) => {
        console.log(response);
        callback(null, {
          city: {
            id: response.data.id,
            name: response.data.name,
            country: {
              id: "234",
              name: "Murica",
            },
          },
          weather: {
            temperature: {
              value: response.data.main.temp,
              unit: "CELCIUS",
            },
          },
          datetime: new Date(response.data.dt * 1000).toISOString(),
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }
};
